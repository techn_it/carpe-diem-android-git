package techn.carpediem;

import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public class RemoteViewsService extends android.widget.RemoteViewsService implements android.widget.RemoteViewsService.RemoteViewsFactory {

    public static final String EXTRA_TRACKABLE_INDEX = "techn.carpediem.EXTRA_TRACKABLE_INDEX";

    private final Storage storage = new Storage();

    private List<Trackable> trackables;

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return this;
    }

    @Override
    public void onDataSetChanged() {
        try {
            trackables = storage.loadTrackables(this);
        } catch (IOException ex) {
            Log.e(TrackablesActivity.class.getSimpleName(), "Couldn't load trackables", ex);
            trackables = Collections.emptyList();
        }
    }

    @Override
    public int getCount() {
        return trackables.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        Trackable trackable = trackables.get(position);
        int elapsed = (int) (trackable.getElapsedBounded(this) * 100);
        int remaining = 100 - elapsed;
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.app_widget_trackable);
        remoteViews.setProgressBar(R.id.progressBar, 100, elapsed, false);
        remoteViews.setTextViewText(R.id.statusTextView, getString(R.string.trackable_status, remaining, trackable.getName(this).toLowerCase()));
        Intent onClickFillInIntent = new Intent();
        onClickFillInIntent.putExtra(EXTRA_TRACKABLE_INDEX, position);
        remoteViews.setOnClickFillInIntent(R.id.rootLayout, onClickFillInIntent);
        return remoteViews;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
