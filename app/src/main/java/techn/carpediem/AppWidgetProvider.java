package techn.carpediem;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class AppWidgetProvider extends android.appwidget.AppWidgetProvider {

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.app_widget_trackables);
        remoteViews.setRemoteAdapter(R.id.listView, new Intent(context, RemoteViewsService.class));
        Intent changeTrackableSettingsIntent = new Intent(context, TrackablesActivity.class);
        changeTrackableSettingsIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent changeTrackableSettingsPendingIntent = PendingIntent.getActivity(context, 0, changeTrackableSettingsIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setPendingIntentTemplate(R.id.listView, changeTrackableSettingsPendingIntent);
        appWidgetManager.updateAppWidget(appWidgetIds, remoteViews);
        appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.listView);
    }
}
