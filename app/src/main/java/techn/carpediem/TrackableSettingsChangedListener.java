package techn.carpediem;

@FunctionalInterface
public interface TrackableSettingsChangedListener {

    void trackableSettingsChanged(Trackable trackable);
}
