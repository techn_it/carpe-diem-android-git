package techn.carpediem;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

import java.util.Calendar;

public class TimePickerDialogFragment extends DateTimePickerDialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar initialTime = getInitialDateTime();
        return new TimePickerDialog(
                getContext(),
                this::timePicked,
                initialTime.get(Calendar.HOUR_OF_DAY),
                initialTime.get(Calendar.MINUTE),
                true);
    }

    private void timePicked(TimePicker timePicker, int hourOfDay, int minute) {
        Calendar date = Calendar.getInstance();
        date.set(Calendar.HOUR_OF_DAY, hourOfDay);
        date.set(Calendar.MINUTE, minute);
        notifyTargetOfDateTimePicked(date);
    }
}
