package techn.carpediem;

import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class LifeSettingsDialogFragment extends DateTimePickerTargetDialogFragment<Life> {

    private DateEditText dateOfBirthEditText;
    private RadioGroup sexRadioGroup;
    private Spinner countrySpinner;

    @Override
    protected Class<Life> getTrackableClass() {
        return Life.class;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.life_settings;
    }

    @Override
    protected void initLayout(View layout, Life life) {

        dateOfBirthEditText = layout.findViewById(R.id.dateOfBirthEditText);
        dateOfBirthEditText.setDateTimePickerTargetDialogFragment(this);
        dateOfBirthEditText.setDateTime(life.getDateOfBirth());

        sexRadioGroup = layout.findViewById(R.id.sexRadioGroup);
        sexRadioGroup.check(life.isMale() ? R.id.maleRadioButton : R.id.femaleRadioButton);

        countrySpinner = layout.findViewById(R.id.countrySpinner);
        ArrayAdapter<CharSequence> countriesAdapter = ArrayAdapter.createFromResource(getContext(), R.array.countries, android.R.layout.simple_spinner_item);
        countriesAdapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        countrySpinner.setAdapter(countriesAdapter);
        countrySpinner.setSelection(life.getCountryIndex());

    }

    @Override
    protected void updateTrackable(Life life, View layout) {
        life.setDateOfBirth(dateOfBirthEditText.getDateTime());
        life.setMale(sexRadioGroup.getCheckedRadioButtonId() == R.id.maleRadioButton);
        life.setCountryIndex(countrySpinner.getSelectedItemPosition());
    }
}
