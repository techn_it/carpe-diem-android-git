package techn.carpediem;

import android.content.Context;
import android.util.AttributeSet;

public class TimeEditText extends DateTimeEditText {

    public TimeEditText(Context context) {
        super(context);
    }

    public TimeEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TimeEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected DateTimePickerDialogFragment createNewDateTimePickerDialogFragment() {
        return new TimePickerDialogFragment();
    }

    @Override
    protected DateTimeFormat getDateTimeFormat() {
        return DateTimeFormat.TIME;
    }
}
