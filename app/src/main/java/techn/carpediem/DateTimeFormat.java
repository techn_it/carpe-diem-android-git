package techn.carpediem;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public enum DateTimeFormat {

    DATE(SimpleDateFormat.getDateInstance(DateFormat.MEDIUM, Locale.UK)),
    TIME(SimpleDateFormat.getTimeInstance(DateFormat.SHORT, Locale.UK));

    private final DateFormat dateTimeFormat;

    DateTimeFormat(DateFormat dateTimeFormat) {
        this.dateTimeFormat = dateTimeFormat;
    }

    public String format(Calendar dateTime) {
        return dateTimeFormat.format(dateTime.getTime());
    }

    public Calendar parse(String dateTimeString) {
        Calendar dateTime = Calendar.getInstance();
        try {
            dateTime.setTime(dateTimeFormat.parse(dateTimeString));
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }
        return dateTime;
    }
}
