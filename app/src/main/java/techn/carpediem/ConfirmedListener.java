package techn.carpediem;

@FunctionalInterface
public interface ConfirmedListener<T> {

    void confirmed(T response);
}
