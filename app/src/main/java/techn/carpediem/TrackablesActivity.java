package techn.carpediem;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.List;

public class TrackablesActivity extends AppCompatActivity implements TrackableSettingsChangedListener, TrackableDeletedListener, TrackableMovedListener {

    public static final String TRACKABLE_ARGUMENT_KEY = "trackable";

    private final Storage storage = new Storage();

    private List<Trackable> trackables;
    private TrackablesAdapter trackablesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_trackables);

        try {
            trackables = storage.loadTrackables(this);
        } catch (IOException ex) {
            Log.e(TrackablesActivity.class.getSimpleName(), "Couldn't load trackables", ex);
            Toast.makeText(this, R.string.couldnt_load_trackables, Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        TrackableItemTouchHelper trackableItemTouchHelper = new TrackableItemTouchHelper(this);
        trackablesAdapter = new TrackablesAdapter(trackables, this::showTrackableSettings, trackableItemTouchHelper);
        RecyclerView trackablesRecyclerView = findViewById(R.id.recyclerView);
        trackablesRecyclerView.setHasFixedSize(true);
        trackablesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        trackablesRecyclerView.setAdapter(trackablesAdapter);
        trackableItemTouchHelper.attachToRecyclerView(trackablesRecyclerView);

        int trackableIndex = getIntent().getIntExtra(RemoteViewsService.EXTRA_TRACKABLE_INDEX, -1);
        if (trackableIndex >= 0) {
            showTrackableSettings(trackables.get(trackableIndex));
        }

    }

    public void addDeadline(View view) {
        Deadline deadline = new Deadline(this);
        trackables.add(deadline);
        trackablesAdapter.notifyItemInserted(trackables.size() - 1);
        storeTrackablesAndUpdateAppWidgets();
        showTrackableSettings(deadline);
    }

    public void showTrackableSettings(Trackable trackable) {
        if (trackable == null) {
            throw new NullPointerException("Attempting to show settings of null trackable");
        }
        if (trackable.getSettingsDialogFragmentClass() == null) {
            return;
        }
        Bundle arguments = new Bundle();
        arguments.putSerializable(TRACKABLE_ARGUMENT_KEY, trackable);
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.getFragmentFactory().instantiate(getClassLoader(), trackable.getSettingsDialogFragmentClass().getName());
        fragment.setArguments(arguments);
        ((DialogFragment) fragment).show(fragmentManager, null);
    }

    @Override
    public void trackableSettingsChanged(Trackable trackable) {
        trackablesAdapter.notifyItemChanged(trackables.indexOf(trackable));
        storeTrackablesAndUpdateAppWidgets();
    }

    @Override
    public void trackableDeleted(Trackable trackable) {
        int index = trackables.indexOf(trackable);
        trackables.remove(index);
        trackablesAdapter.notifyItemRemoved(index);
        storeTrackablesAndUpdateAppWidgets();
    }

    @Override
    public void trackableMoved(Trackable trackable, int newIndex) {
        int oldIndex = trackables.indexOf(trackable);
        trackables.remove(oldIndex);
        trackables.add(newIndex, trackable);
        trackablesAdapter.notifyItemMoved(oldIndex, newIndex);
        storeTrackablesAndUpdateAppWidgets();
    }

    private void storeTrackablesAndUpdateAppWidgets() {
        try {
            storage.storeTrackables(this, trackables);
            int[] appWidgetIds = AppWidgetManager.getInstance(this).getAppWidgetIds(new ComponentName(this, AppWidgetProvider.class));
            AppWidgetManager.getInstance(this).notifyAppWidgetViewDataChanged(appWidgetIds, R.id.listView);
        } catch (IOException ex) {
            Log.e(TrackablesActivity.class.getSimpleName(), "Couldn't store trackables", ex);
            Toast.makeText(this, R.string.couldnt_store_trackables, Toast.LENGTH_LONG).show();
        }
    }
}
