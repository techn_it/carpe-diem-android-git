package techn.carpediem;

import android.app.Dialog;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

public class ConfirmationDialogFragment<T> extends DialogFragment {

    public static final String TITLE_ARGUMENT_KEY = "title";
    public static final String RESPONSE_ARGUMENT_KEY = "response";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getContext())
                .setTitle(getArguments().getString(TITLE_ARGUMENT_KEY))
                .setMessage(R.string.are_you_certain)
                .setPositiveButton(R.string.yes, ((dialog, button) -> notifyTargetOfConfirmation((T) getArguments().getSerializable(RESPONSE_ARGUMENT_KEY))))
                .setNegativeButton(R.string.no, null)
                .create();
    }

    private void notifyTargetOfConfirmation(T response) {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment instanceof ConfirmedListener) {
            ((ConfirmedListener<T>) targetFragment).confirmed(response);
        } else {
            throw new IllegalStateException("Target fragment does not implement ConfirmedListener");
        }
    }
}
