package techn.carpediem;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class TrackableViewHolder extends RecyclerView.ViewHolder {

    public TrackableViewHolder(View trackableView) {
        super(trackableView);
    }

    public View getTrackableView() {
        return itemView;
    }
}
