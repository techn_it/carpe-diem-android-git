package techn.carpediem;

import java.util.Calendar;

public abstract class DateTimePickerTargetDialogFragment<T extends Trackable> extends SettingsDialogFragment<T> {

    public void dateTimePicked(Calendar dateTime, int targetViewId) {
        getDialog().<DateTimeEditText>findViewById(targetViewId).setDateTime(dateTime);
    }
}
