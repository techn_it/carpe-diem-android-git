package techn.carpediem;

import android.content.Context;
import android.util.Log;

import java.util.Calendar;

import androidx.fragment.app.DialogFragment;

public class Period extends Trackable {

    private final PeriodType periodType;

    public Period(PeriodType periodType) {
        this.periodType = periodType;
    }

    @Override
    public String getName(Context context) {
        return context.getString(periodType.getNameStringResource());
    }

    @Override
    protected double getElapsed(Context context) {
        Calendar now = Calendar.getInstance();
        Calendar startOfCurrentPeriod;
        Calendar startOfNextPeriod;
        switch (periodType) {
            case YEAR:
                startOfCurrentPeriod = (Calendar) now.clone();
                startOfCurrentPeriod.set(Calendar.DAY_OF_YEAR, 1);
                startOfCurrentPeriod.set(Calendar.HOUR_OF_DAY, 0);
                startOfCurrentPeriod.set(Calendar.MINUTE, 0);
                startOfCurrentPeriod.set(Calendar.SECOND, 0);
                startOfCurrentPeriod.set(Calendar.MILLISECOND, 0);
                startOfNextPeriod = (Calendar) startOfCurrentPeriod.clone();
                startOfNextPeriod.add(Calendar.YEAR, 1);
                break;
            case MONTH:
                startOfCurrentPeriod = (Calendar) now.clone();
                startOfCurrentPeriod.set(Calendar.DAY_OF_MONTH, 1);
                startOfCurrentPeriod.set(Calendar.HOUR_OF_DAY, 0);
                startOfCurrentPeriod.set(Calendar.MINUTE, 0);
                startOfCurrentPeriod.set(Calendar.SECOND, 0);
                startOfCurrentPeriod.set(Calendar.MILLISECOND, 0);
                startOfNextPeriod = (Calendar) startOfCurrentPeriod.clone();
                startOfNextPeriod.add(Calendar.MONTH, 1);
                break;
            case WEEK:
                startOfCurrentPeriod = (Calendar) now.clone();
                startOfCurrentPeriod.set(Calendar.DAY_OF_WEEK, now.getFirstDayOfWeek());
                startOfCurrentPeriod.set(Calendar.HOUR_OF_DAY, 0);
                startOfCurrentPeriod.set(Calendar.MINUTE, 0);
                startOfCurrentPeriod.set(Calendar.SECOND, 0);
                startOfCurrentPeriod.set(Calendar.MILLISECOND, 0);
                startOfNextPeriod = (Calendar) startOfCurrentPeriod.clone();
                startOfNextPeriod.add(Calendar.WEEK_OF_YEAR, 1);
                break;
            case DAY:
                startOfCurrentPeriod = (Calendar) now.clone();
                startOfCurrentPeriod.set(Calendar.HOUR_OF_DAY, 0);
                startOfCurrentPeriod.set(Calendar.MINUTE, 0);
                startOfCurrentPeriod.set(Calendar.SECOND, 0);
                startOfCurrentPeriod.set(Calendar.MILLISECOND, 0);
                startOfNextPeriod = (Calendar) startOfCurrentPeriod.clone();
                startOfNextPeriod.add(Calendar.DAY_OF_MONTH, 1);
                break;
            default:
                throw new UnsupportedOperationException("Unknown period type");
        }
        Log.d(Period.class.getSimpleName(), String.format("%s: %tc – %tc – %tc", periodType, startOfCurrentPeriod, now, startOfNextPeriod));
        return (double) (now.getTimeInMillis() - startOfCurrentPeriod.getTimeInMillis()) / (startOfNextPeriod.getTimeInMillis() - startOfCurrentPeriod.getTimeInMillis());
    }

    @Override
    public Class<? extends DialogFragment> getSettingsDialogFragmentClass() {
        return null;
    }

    @Override
    public boolean isDeletable() {
        return false;
    }
}
