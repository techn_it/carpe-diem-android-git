package techn.carpediem;

import android.content.Context;
import android.content.res.TypedArray;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class Life extends Trackable {

    private static final int UNITED_STATES_COUNTRY_INDEX = 174;

    private Calendar dateOfBirth;
    private boolean male = true;
    private int countryIndex = UNITED_STATES_COUNTRY_INDEX;

    public Life() {
        dateOfBirth = Calendar.getInstance();
        dateOfBirth.add(Calendar.YEAR, -30);
        clearTime(dateOfBirth);
    }

    public Calendar getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Calendar dateOfBirth) {
        clearTime(this.dateOfBirth = dateOfBirth);
    }

    private void clearTime(Calendar dateTime) {
        dateTime.set(Calendar.HOUR_OF_DAY, 0);
        dateTime.set(Calendar.MINUTE, 0);
        dateTime.set(Calendar.SECOND, 0);
        dateTime.set(Calendar.MILLISECOND, 0);
    }

    public boolean isMale() {
        return male;
    }

    public void setMale(boolean male) {
        this.male = male;
    }

    public int getCountryIndex() {
        return countryIndex;
    }

    public void setCountryIndex(int countryIndex) {
        this.countryIndex = countryIndex;
    }

    @Override
    public String getName(Context context) {
        return context.getString(R.string.life);
    }

    @Override
    public double getElapsed(Context context) {
        TypedArray lifeExpectancies = context.getResources().obtainTypedArray(isMale() ? R.array.male_life_expectancies : R.array.female_life_expectancies);
        double lifeExpectancyYears = lifeExpectancies.getFloat(getCountryIndex(), 0);
        lifeExpectancies.recycle();
        double lifeElapsedYears = (System.currentTimeMillis() - getDateOfBirth().getTimeInMillis()) / 1_000D / 60 / 60 / 24 / 365;
        return lifeElapsedYears / lifeExpectancyYears;
    }

    @Override
    public Class<? extends DialogFragment> getSettingsDialogFragmentClass() {
        return LifeSettingsDialogFragment.class;
    }

    @Override
    public boolean isDeletable() {
        return false;
    }
}
