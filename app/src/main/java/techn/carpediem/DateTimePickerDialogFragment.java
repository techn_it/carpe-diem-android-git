package techn.carpediem;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import java.util.Calendar;

public abstract class DateTimePickerDialogFragment extends DialogFragment {

    public static final String DATE_TIME_ARGUMENT_KEY = "date_time";
    public static final String TARGET_VIEW_ID_KEY = "target_view_id";

    protected Calendar getInitialDateTime() {
        Calendar initialDateTime = (Calendar) getArguments().getSerializable(DATE_TIME_ARGUMENT_KEY);
        return initialDateTime == null ? Calendar.getInstance() : initialDateTime;
    }

    protected void notifyTargetOfDateTimePicked(Calendar dateTime) {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment instanceof DateTimePickerTargetDialogFragment<?>) {
            ((DateTimePickerTargetDialogFragment<?>) targetFragment).dateTimePicked(dateTime, getArguments().getInt(TARGET_VIEW_ID_KEY));
        } else {
            throw new IllegalStateException("Target fragment is not a picker target");
        }
    }
}
