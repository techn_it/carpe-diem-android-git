package techn.carpediem;

@FunctionalInterface
public interface TrackableDeletedListener {

    void trackableDeleted(Trackable trackable);
}
