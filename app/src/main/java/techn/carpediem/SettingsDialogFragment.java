package techn.carpediem;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.io.Serializable;

public abstract class SettingsDialogFragment<T extends Trackable> extends DialogFragment implements ConfirmedListener<T> {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        T trackable = getTrackable();
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View layout = layoutInflater.inflate(
                getLayoutResource(),
                (ViewGroup) layoutInflater.inflate(R.layout.settings, null),
                true);
        initLayout(layout, trackable);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext())
                .setTitle(trackable.getName(getContext()))
                .setView(layout)
                .setPositiveButton(R.string.ok, (dialog, button) -> {
                    updateTrackable(trackable, layout);
                    notifyActivityOfTrackableSettingsChanged(trackable);
                })
                .setNegativeButton(R.string.cancel, null);
        if (trackable.isDeletable()) {
            AlertDialog alertDialog = alertDialogBuilder
                    .setNeutralButton(R.string.delete, null)
                    .create();
            /* Button's OnClickListener needs to be set separately so that the dialog isn't
             *  automatically dismissed when that button is clicked */
            alertDialog.setOnShowListener(dialog -> {
                ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(button -> {
                    Bundle arguments = new Bundle();
                    arguments.putString(ConfirmationDialogFragment.TITLE_ARGUMENT_KEY, getString(R.string.delete_trackable, trackable.getName(getContext())));
                    arguments.putSerializable(ConfirmationDialogFragment.RESPONSE_ARGUMENT_KEY, trackable);
                    DialogFragment confirmationDialogFragment = new ConfirmationDialogFragment<T>();
                    confirmationDialogFragment.setArguments(arguments);
                    confirmationDialogFragment.setTargetFragment(this, 0);
                    confirmationDialogFragment.show(getFragmentManager(), null);
                });
            });
            return alertDialog;
        }
        return alertDialogBuilder.create();
    }

    private T getTrackable() {
        Class<T> trackableClass = getTrackableClass();
        Serializable argument = getArguments().getSerializable(TrackablesActivity.TRACKABLE_ARGUMENT_KEY);
        if (!trackableClass.isInstance(argument)) {
            throw new IllegalArgumentException("Invalid argument passed");
        }
        return trackableClass.cast(argument);
    }

    protected abstract Class<T> getTrackableClass();

    protected abstract int getLayoutResource();

    protected abstract void initLayout(View layout, T trackable);

    protected abstract void updateTrackable(T trackable, View layout);

    private void notifyActivityOfTrackableSettingsChanged(Trackable trackable) {
        if (getActivity() instanceof TrackableSettingsChangedListener) {
            ((TrackableSettingsChangedListener) getActivity()).trackableSettingsChanged(trackable);
        } else {
            throw new IllegalStateException("Associated activity doesn't implement TrackableSettingsChangedListener");
        }
    }

    @Override
    public void confirmed(T trackable) {
        getDialog().dismiss();
        if (getActivity() instanceof TrackableDeletedListener) {
            ((TrackableDeletedListener) getActivity()).trackableDeleted(trackable);
        } else {
            throw new IllegalStateException("Associated activity doesn't implement TrackableDeletedListener");
        }
    }
}
