package techn.carpediem;

import android.content.Context;
import android.util.AttributeSet;

public class DateEditText extends DateTimeEditText {

    public DateEditText(Context context) {
        super(context);
    }

    public DateEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DateEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected DateTimePickerDialogFragment createNewDateTimePickerDialogFragment() {
        return new DatePickerDialogFragment();
    }

    @Override
    protected DateTimeFormat getDateTimeFormat() {
        return DateTimeFormat.DATE;
    }
}
