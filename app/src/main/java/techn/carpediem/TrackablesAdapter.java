package techn.carpediem;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class TrackablesAdapter extends RecyclerView.Adapter<TrackableViewHolder> {

    private final List<Trackable> trackables;
    private final TrackableSelectedListener trackableSelectedListener;
    private final TrackableItemTouchHelper trackableItemTouchHelper;

    public TrackablesAdapter(List<Trackable> trackables, TrackableSelectedListener trackableSelectedListener, TrackableItemTouchHelper trackableItemTouchHelper) {
        this.trackables = trackables;
        this.trackableSelectedListener = trackableSelectedListener;
        this.trackableItemTouchHelper = trackableItemTouchHelper;
    }

    public Trackable getTrackable(int position) {
        return trackables.get(position);
    }

    @Override
    public TrackableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View trackableView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_trackable, parent, false);
        return new TrackableViewHolder(trackableView);
    }

    @Override
    public void onBindViewHolder(TrackableViewHolder trackableViewHolder, int position) {
        View trackableView = trackableViewHolder.getTrackableView();
        Context context = trackableView.getContext();
        Trackable trackable = getTrackable(position);
        int elapsed = (int) (trackable.getElapsedBounded(context) * 100);
        int remaining = 100 - elapsed;
        trackableView.<ProgressBar>findViewById(R.id.progressBar).setProgress(elapsed);
        trackableView.<ImageView>findViewById(R.id.dragIndicatorImageView).setOnTouchListener((dragIndicatorImageView, motionEvent) -> {
            if (motionEvent.getActionMasked() == MotionEvent.ACTION_DOWN) {
                trackableItemTouchHelper.startDrag(trackableViewHolder);
                return true;
            }
            return false;
        });
        trackableView.<TextView>findViewById(R.id.statusTextView).setText(context.getString(R.string.trackable_status, remaining, trackable.getName(context).toLowerCase()));
        ImageView settingsImageView = trackableView.findViewById(R.id.settingsImageView);
        if (trackable.getSettingsDialogFragmentClass() != null) {
            trackableView.setOnClickListener(lambdaTrackableView -> {
                ((Animatable) settingsImageView.getDrawable()).start();
                if (trackableSelectedListener != null) {
                    trackableSelectedListener.trackableSelected(trackable);
                }
            });
            settingsImageView.setVisibility(View.VISIBLE);
        } else {
            trackableView.setOnClickListener(null);
            settingsImageView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return trackables.size();
    }
}
