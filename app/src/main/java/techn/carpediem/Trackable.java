package techn.carpediem;

import android.content.Context;

import androidx.fragment.app.DialogFragment;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public abstract class Trackable implements Serializable {

    private final UUID uuid = UUID.randomUUID();

    public abstract String getName(Context context);

    protected abstract double getElapsed(Context context);

    public final double getElapsedBounded(Context context) {
        return Math.min(1, Math.max(0, getElapsed(context)));
    }

    public abstract Class<? extends DialogFragment> getSettingsDialogFragmentClass();

    public abstract boolean isDeletable();

    @Override
    public boolean equals(Object other) {
        return other instanceof Trackable && Objects.equals(((Trackable) other).uuid, uuid);
    }
}
