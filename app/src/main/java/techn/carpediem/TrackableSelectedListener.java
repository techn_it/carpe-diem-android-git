package techn.carpediem;

@FunctionalInterface
public interface TrackableSelectedListener {

    void trackableSelected(Trackable trackable);
}
