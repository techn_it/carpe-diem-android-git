package techn.carpediem;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.text.InputType;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import java.util.Calendar;

public abstract class DateTimeEditText extends AppCompatEditText {

    private DateTimePickerTargetDialogFragment<?> dateTimePickerTargetDialogFragment;
    private Calendar dateTime;

    public DateTimeEditText(Context context) {
        super(context);
        init();
    }

    public DateTimeEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DateTimeEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setBackgroundResource(0);
        setFocusable(false);
        setInputType(InputType.TYPE_NULL);
        setOnClickListener(view -> {
            if (dateTimePickerTargetDialogFragment == null) {
                throw new IllegalStateException("Picker target fragment not set");
            }
            showPicker();
        });
    }

    private void showPicker() {
        Bundle arguments = new Bundle();
        arguments.putSerializable(DateTimePickerDialogFragment.DATE_TIME_ARGUMENT_KEY, getDateTime());
        arguments.putInt(DateTimePickerDialogFragment.TARGET_VIEW_ID_KEY, getId());
        DateTimePickerDialogFragment dateTimePickerDialogFragment = createNewDateTimePickerDialogFragment();
        dateTimePickerDialogFragment.setArguments(arguments);
        dateTimePickerDialogFragment.setTargetFragment(getDateTimePickerTargetDialogFragment(), 0);
        dateTimePickerDialogFragment.show(getFragmentManager(), null);
    }

    protected abstract DateTimePickerDialogFragment createNewDateTimePickerDialogFragment();

    private FragmentManager getFragmentManager() {
        Context context = getContext();
        while (true) {
            if (context instanceof FragmentActivity) {
                return ((FragmentActivity) context).getSupportFragmentManager();
            }
            if (context instanceof ContextWrapper) {
                context = ((ContextWrapper) context).getBaseContext();
                continue;
            }
            throw new UnsupportedOperationException("FragmentActivity context not found");
        }
    }

    public DateTimePickerTargetDialogFragment<?> getDateTimePickerTargetDialogFragment() {
        return dateTimePickerTargetDialogFragment;
    }

    public void setDateTimePickerTargetDialogFragment(DateTimePickerTargetDialogFragment<?> dateTimePickerTargetDialogFragment) {
        this.dateTimePickerTargetDialogFragment = dateTimePickerTargetDialogFragment;
    }

    public Calendar getDateTime() {
        return dateTime;
    }

    public void setDateTime(Calendar dateTime) {
        this.dateTime = dateTime;
        setText(getDateTimeFormat().format(dateTime));
    }

    protected abstract DateTimeFormat getDateTimeFormat();
}
