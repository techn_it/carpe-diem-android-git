package techn.carpediem;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import java.util.Calendar;

public class DeadlineSettingsDialogFragment extends DateTimePickerTargetDialogFragment<Deadline> {

    private EditText nameEditText;
    private DateEditText startDateEditText;
    private TimeEditText startTimeEditText;
    private DateEditText endDateEditText;
    private TimeEditText endTimeEditText;

    @Override
    protected Class<Deadline> getTrackableClass() {
        return Deadline.class;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.deadline_settings;
    }

    @Override
    protected void initLayout(View layout, Deadline deadline) {

        nameEditText = layout.findViewById(R.id.nameEditText);
        nameEditText.setText(deadline.getName(getContext()));
        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                getDialog().setTitle(s.toString());
            }
        });

        Calendar startDateTime = deadline.getStartDateTime();

        startDateEditText = layout.findViewById(R.id.startDateEditText);
        startDateEditText.setDateTime(startDateTime);
        startDateEditText.setDateTimePickerTargetDialogFragment(this);

        startTimeEditText = layout.findViewById(R.id.startTimeEditText);
        startTimeEditText.setDateTime(startDateTime);
        startTimeEditText.setDateTimePickerTargetDialogFragment(this);

        Calendar endDateTime = deadline.getEndDateTime();

        endDateEditText = layout.findViewById(R.id.endDateEditText);
        endDateEditText.setDateTime(endDateTime);
        endDateEditText.setDateTimePickerTargetDialogFragment(this);

        endTimeEditText = layout.findViewById(R.id.endTimeEditText);
        endTimeEditText.setDateTime(endDateTime);
        endTimeEditText.setDateTimePickerTargetDialogFragment(this);

    }

    @Override
    protected void updateTrackable(Deadline deadline, View layout) {
        deadline.setName(nameEditText.getText().toString());
        deadline.setStartDateTime(combineDateAndTime(startDateEditText.getDateTime(), startTimeEditText.getDateTime()));
        deadline.setEndDateTime(combineDateAndTime(endDateEditText.getDateTime(), endTimeEditText.getDateTime()));
    }

    private Calendar combineDateAndTime(Calendar date, Calendar time) {
        Calendar dateTime = (Calendar) time.clone();
        dateTime.set(date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH));
        return dateTime;
    }
}
