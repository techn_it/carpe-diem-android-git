package techn.carpediem;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

public class TrackableItemTouchHelper extends ItemTouchHelper {

    public TrackableItemTouchHelper(TrackableMovedListener trackableMovedListener) {
        super(new SimpleCallback(UP | DOWN, 0) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder draggedViewHolder, RecyclerView.ViewHolder targetViewHolder) {
                Trackable draggedTrackable = ((TrackablesAdapter) recyclerView.getAdapter()).getTrackable(draggedViewHolder.getAdapterPosition());
                int newPosition = targetViewHolder.getAdapterPosition();
                trackableMovedListener.trackableMoved(draggedTrackable, newPosition);
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            }

            @Override
            public boolean isLongPressDragEnabled() {
                return false;
            }
        });
    }
}
