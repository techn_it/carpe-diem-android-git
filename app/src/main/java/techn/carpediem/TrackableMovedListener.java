package techn.carpediem;

@FunctionalInterface
public interface TrackableMovedListener {

    void trackableMoved(Trackable trackable, int newIndex);
}
