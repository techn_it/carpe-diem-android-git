package techn.carpediem;

import android.content.Context;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class Storage {

    private static final String FILENAME = "trackables";

    @SuppressWarnings("unchecked")
    public List<Trackable> loadTrackables(Context context) throws IOException {
        try (ObjectInputStream objectInputStream = new ObjectInputStream(context.openFileInput(FILENAME))) {
            return (List<Trackable>) objectInputStream.readObject();
        } catch (FileNotFoundException ex) {
            List<Trackable> defaultTrackables = getDefaultTrackables();
            storeTrackables(context, defaultTrackables);
            return defaultTrackables;
        } catch (ClassNotFoundException ex) {
            throw new AssertionError(ex);
        }
    }

    private List<Trackable> getDefaultTrackables() {
        List<Trackable> defaultTrackables = new ArrayList<>();
        defaultTrackables.add(new Life());
        for (PeriodType periodType : PeriodType.values()) {
            defaultTrackables.add(new Period(periodType));
        }
        return defaultTrackables;
    }

    public void storeTrackables(Context context, List<Trackable> trackables) throws IOException {
        if (trackables == null) {
            throw new NullPointerException("Trackables cannot be null");
        }
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(context.openFileOutput(FILENAME, Context.MODE_PRIVATE))) {
            objectOutputStream.writeObject(trackables);
        }
    }
}
