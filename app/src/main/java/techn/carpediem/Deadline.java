package techn.carpediem;

import android.content.Context;

import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class Deadline extends Trackable {

    private String name;
    private Calendar startDateTime;
    private Calendar endDateTime;

    public Deadline(Context context) {

        name = context.getString(R.string.deadline);

        startDateTime = Calendar.getInstance();
        clearSecondsAndMilliseconds(startDateTime);

        endDateTime = (Calendar) startDateTime.clone();
        endDateTime.add(Calendar.YEAR, 1);

    }

    @Override
    public String getName(Context context) {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(Calendar startDateTime) {
        clearSecondsAndMilliseconds(this.startDateTime = startDateTime);
    }

    public Calendar getEndDateTime() {
        return endDateTime;
    }

    public void setEndDateTime(Calendar endDateTime) {
        clearSecondsAndMilliseconds(this.endDateTime = endDateTime);
    }

    private void clearSecondsAndMilliseconds(Calendar dateTime) {
        dateTime.set(Calendar.SECOND, 0);
        dateTime.set(Calendar.MILLISECOND, 0);
    }

    @Override
    protected double getElapsed(Context context) {
        return (double) (System.currentTimeMillis() - getStartDateTime().getTimeInMillis())
                / (getEndDateTime().getTimeInMillis() - getStartDateTime().getTimeInMillis());
    }

    @Override
    public Class<? extends DialogFragment> getSettingsDialogFragmentClass() {
        return DeadlineSettingsDialogFragment.class;
    }

    @Override
    public boolean isDeletable() {
        return true;
    }
}
