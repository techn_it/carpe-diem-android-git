package techn.carpediem;

public enum PeriodType {

    YEAR(R.string.year),
    MONTH(R.string.month),
    WEEK(R.string.week),
    DAY(R.string.day);

    private final int nameStringResource;

    PeriodType(int nameStringResource) {
        this.nameStringResource = nameStringResource;
    }

    public int getNameStringResource() {
        return nameStringResource;
    }
}
